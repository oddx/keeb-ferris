#include QMK_KEYBOARD_H

enum ferris_layers {
  QWERTY,
  NUM,
  FUNCTION
};

enum {
    TF1,
    TF2,
    TF3,
    TF4,
    TF5,
    TF6,
    TF7,
    TF8,
    TF9,
    TF10,
    TF11,
    TF12,
};

tap_dance_action_t tap_dance_actions[] = {
    [TF1]  = ACTION_TAP_DANCE_DOUBLE(KC_F1,  LCA(KC_F1)),
    [TF2]  = ACTION_TAP_DANCE_DOUBLE(KC_F2,  LCA(KC_F2)),
    [TF3]  = ACTION_TAP_DANCE_DOUBLE(KC_F3,  LCA(KC_F3)),
    [TF4]  = ACTION_TAP_DANCE_DOUBLE(KC_F4,  LCA(KC_F4)),
    [TF5]  = ACTION_TAP_DANCE_DOUBLE(KC_F5,  LCA(KC_F5)),
    [TF6]  = ACTION_TAP_DANCE_DOUBLE(KC_F6,  LCA(KC_F6)),
    [TF7]  = ACTION_TAP_DANCE_DOUBLE(KC_F7,  LCA(KC_F7)),
    [TF8]  = ACTION_TAP_DANCE_DOUBLE(KC_F8,  LCA(KC_F8)),
    [TF9]  = ACTION_TAP_DANCE_DOUBLE(KC_F9,  LCA(KC_F9)),
    [TF10] = ACTION_TAP_DANCE_DOUBLE(KC_F10, LCA(KC_F10)),
    [TF11] = ACTION_TAP_DANCE_DOUBLE(KC_F11, LCA(KC_F11)),
    [TF12] = ACTION_TAP_DANCE_DOUBLE(KC_F12, LCA(KC_F12)),
};

#define KC_CTSL RCTL_T(KC_SLSH)
#define KC_CTLZ LCTL_T(KC_Z)
#define KC_ALTX LALT_T(KC_X)
#define KC_ALTD RALT_T(KC_DOT)
#define KC_LSHA LSFT_T(KC_A)
#define KC_SHSC RSFT_T(KC_SCLN)
#define KC_GUIC GUI_T(KC_C)
#define KC_GCOM GUI_T(KC_COMM)
#define KC_GPLY GUI_T(KC_MPLY)
#define KC_ARWD LALT_T(KC_MRWD)
#define LT_1ESC LT(1,KC_ESC)
#define LT_2ENT LT(2,KC_ENT)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [QWERTY] = LAYOUT(
    KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,            KC_Y,    KC_U,   KC_I,    KC_O,    KC_P,
    KC_LSHA, KC_S,    KC_D,    KC_F,    KC_G,            KC_H,    KC_J,   KC_K,    KC_L,    KC_SHSC,
    KC_CTLZ, KC_ALTX, KC_GUIC, KC_V,    KC_B,            KC_N,    KC_M,   KC_GCOM, KC_ALTD, KC_CTSL,
                                   LT_1ESC, KC_BSPC, KC_SPC,     LT_2ENT
  ),
  
  [NUM] = LAYOUT(
    KC_ESC,  KC_VOLD, KC_UP,   KC_VOLU, KC_QUOT,         KC_LBRC, KC_1,   KC_2,    KC_3,    KC_MINS,
    KC_TRNS, KC_LEFT, KC_DOWN, KC_RGHT, KC_GRV,          KC_BSLS, KC_4,   KC_5,    KC_6,    KC_0,
    KC_LCTL, KC_MRWD, KC_MPLY, KC_MFFD, KC_LGUI,         KC_RBRC, KC_7,   KC_8,    KC_9,    KC_EQL,
                                    KC_TRNS, KC_TRNS, KC_TAB,     KC_PSCR
  ),
  
  [FUNCTION] = LAYOUT(
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,         TD(TF1), TD(TF2), TD(TF3), TD(TF4), TD(TF5),
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,         TD(TF6), TD(TF7), TD(TF8), TD(TF9), TD(TF10),
    KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,         TD(TF11),TD(TF12),KC_TRNS, KC_TRNS, KC_TRNS,
                                    KC_TRNS, KC_TRNS, KC_TRNS,    KC_TRNS
  ),
};

void keyboard_post_init_user(void) {
   setPinOutput(F0);
   writePin(F0, 0);
}
